import os
import sys
import argparse
from flask import Flask, render_template, request, jsonify, send_file, flash, redirect, url_for
from werkzeug.utils import secure_filename
import dataframe_manager as dfm
import config
import json
from flask_cors import CORS
from waitress import serve
from shutil import copyfile

# parser = argparse.ArgumentParser()
# parser.add_argument("--production", help="Runs application in production", action="store_true")
# args = parser.parse_args()

if False: #args.production:
    print("Running in production.")
    HOST = os.environ['HOST']
    PORT = os.environ['PORT']
    DEBUG = os.environ['DEBUG']
    DATA_FOLDER = os.environ['DATA_FOLDER']
    FIG_FILENAME = os.environ['FIG_FILENAME']
    CONF_FILENAME = os.environ['CONF_FILENAME']
else:
    print("Running in development.")
    HOST = config.ASS_DOMAIN_CONFIG['host']
    PORT = config.ASS_DOMAIN_CONFIG['port']
    DEBUG = config.ASS_DOMAIN_CONFIG['debug']
    DATA_FOLDER = config.ASS_DOMAIN_CONFIG['data_folder']
    FIG_FILENAME = config.ASS_DOMAIN_CONFIG['fig_filename']
    CONF_FILENAME = config.ASS_DOMAIN_CONFIG['conf_filename']

# Create the application instance
app = Flask(__name__, template_folder="templates")
app.secret_key = "super secret key"
CORS(app)

# Create a URL route in our application for "/"
@app.route('/')
def home():
    """
    Index route.
    :return: The rendered template 'home.html'
    """
    return render_template('home.html')

@app.route('/dataset', methods=['GET', 'POST', 'DELETE'])
def manage_dataset():
    """
    Receives dataset from frontend and saves it to be accessed from dataframe_manager
    :return:
    """
    if request.method == 'POST':
        file = request.files['file']
        if file:
            for f in os.listdir(DATA_FOLDER):
                os.remove(os.path.join(DATA_FOLDER, f))
            file.save(os.path.join(DATA_FOLDER, file.filename))
        return jsonify('OK ma poule')

    elif request.method == 'GET':
        if len(os.listdir(DATA_FOLDER)) > 0:
            size = os.path.getsize(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0]))
            if size > 1073741824:
                size = '%.3f GB' % (size / 1073741824)
            elif size > 1048576:
                size = '%.3f MB' % (size / 1048576)
            elif size > 1024:
                size = '%.3f kB' % (size / 1048576)
            else:
                size = '%f B' % (size)

            return jsonify({ 
                'name': os.listdir(DATA_FOLDER)[0],
                'size': size,
                'rows': dfm.get_shape(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0]))[0],
                'cols': dfm.get_shape(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0]))[1]
            })

    elif request.method == 'DELETE':
        for f in os.listdir(DATA_FOLDER):
            os.remove(os.path.join(DATA_FOLDER, f))

@app.route('/conf', methods=['GET', 'POST'])
def save_conf():
    """
    Receives conf from frontend and saves it.
    :return:
    """
    #conf = request.form.get('configuration')
    if request.method == 'POST':
        conf = request.form.get('configuration')
        my_json = json.loads(conf)
        with open(CONF_FILENAME, 'w') as file:
            #file.write(conf)
            json.dump(my_json, file)

        return jsonify('OK ma poule')

    elif request.method == 'GET':
        with open(CONF_FILENAME, 'r') as file:
            res = json.load(file)
            return jsonify(res)

@app.route('/style', methods=['POST'])
def style():
    style = request.get_json()
    if style['style'] == 'dark':
        os.remove('./light.mplstyle')
    elif style['style'] == 'light':
        copyfile('styles/light.mplstyle', 'light.mplstyle')

    return jsonify(style)

@app.route('/head', methods=['GET'])
def head():
    """
    :return: Head of the dataframe
    """

    return dfm.get_head(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0])).to_json(orient='records')

@app.route('/cols', methods=['GET'])
def cols():
    """
    :return: List of columns
    """

    return jsonify(dfm.get_cols(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0])))

@app.route('/categories', methods=['GET'])
def categories():
    """
    :return: Array of unique categories for column x
    """
    categories = dfm.get_categories(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0]), request.args.get('x'))
    
    return jsonify(categories)

@app.route('/hist', methods=['GET'])
def hist():
    """
    :return: PNG histogram plot
    """
    #os.remove(FIG_FILENAME)
    hist = dfm.get_hist(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0]),
                        FIG_FILENAME,
                        request.args.get('col'),
                        request.args.get('bins'))
    
    return send_file(hist, mimetype='image/png')

@app.route('/plot', methods=['GET'])
def plot():
    """
    :return: PNG plot
    """
    #os.remove(FIG_FILENAME)
    hist = dfm.get_plot(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0]),
                        FIG_FILENAME, 
                        request.args.get('x'),
                        request.args.get('y'),
                        request.args.get('color'),
                        request.args.get('linestyle'),
                        request.args.get('marker'))
    
    return send_file(hist, mimetype='image/png')

@app.route('/boxplot', methods=['GET'])
def boxplot():
    """
    :return: PNG plot
    """
    #os.remove(FIG_FILENAME)
    box = dfm.get_boxplot(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0]),
                        FIG_FILENAME, 
                        request.args.get('x'),
                        request.args.get('y'),
                        request.args.get('categories'),
                        request.args.get('ymin'),
                        request.args.get('ymax'))

    #return jsonify(box)
    return send_file(box, mimetype='image/png')

@app.route('/infos', methods=['GET'])
def infos():
    """
    :return: Description of the column
    """

    return jsonify(dfm.get_infos(os.path.join(DATA_FOLDER, os.listdir(DATA_FOLDER)[0]),
                                request.args.get('col')))

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    #app.run(host=HOST, port=PORT, debug=DEBUG, threaded=True)
    serve(app, host='0.0.0.0', port=8000, threads=1)