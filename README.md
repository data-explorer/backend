# Data Absolver backend

This project is the backend for the app Data Absolver.
It serves a web service (Flask-based)

## Prerequisites 

Python v 3.6.x should be installed on the running machine.

## Set up a virtual env

Set up a Python virtual env in which to install the project dependencies :

```
python -m venv my_venv_name
```

Then depending on your OS, select the newly created environment : 

```
source my_venv_name/bin/activate # Linux
my_venv_name\Scripts\activate.bat # Windows
```

Install the dependencies :

```
pip install -r requirements.txt
```

## Edit the server

As Flask is not stable out of development mode, please use a real wsgi server such as :

  * Gunicorn (Linux)
  * Waitress (windows)

Both modules come install through the `requirements.txt`.
By default, the server is configured to be run on with waitress (Windows), but you may have to edit `server.py` to suit your needs.

*TODO Mettre le bout de code pour Gunicorn.*


## Run the project

Depending on your platform, run the app with the command : 

```
python server.py # Waitress on Windows
gunicorn -w 1 server:app # Gunicorn on Linux
```