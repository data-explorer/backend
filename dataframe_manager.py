import sys
import os
import pandas as pd
from pandas.api.types import is_string_dtype, is_numeric_dtype
import pickle
import json
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from jupyterthemes import jtplot

def apply_style():
    if os.path.isfile('light.mplstyle'):
        mpl.rcParams.update(mpl.rcParamsDefault)
        plt.style.use('light.mplstyle')
    else:
        mpl.rcParams.update(mpl.rcParamsDefault)
        jtplot.style(theme='onedork')


def get_dataframe(f, encoding='latin1'):
    content = pd.read_csv(f, encoding=encoding)

    return content


def get_head(f):
    return get_dataframe(f).head()


def get_shape(f):
    return get_dataframe(f).shape


def get_cols(f):
    return list(get_dataframe(f).columns.values)


def get_categories(f, x):
    return list(get_dataframe(f)[x].dropna().unique())


def get_hist(f, out, col, bins=None):
    fig = plt.figure()
    apply_style()

    if is_numeric_dtype(get_dataframe(f)[col]):
        if int(bins) == 0: 
            bins = get_dataframe(f)[col].max()
        plt.hist(get_dataframe(f)[col], bins=int(bins))
    elif is_string_dtype(get_dataframe(f)[col]):
        get_dataframe(f)[col].value_counts().plot(kind='bar')
    
    fig.savefig(out, format='png')
    plt.close(fig)

    return out


def get_plot(f, out, x, y, c, l, m):
    fig = plt.figure()
    apply_style()

    plt.plot(get_dataframe(f)[x], get_dataframe(f)[y], color=c, linestyle=l, marker=m)
    
    fig.savefig(out, format='png')
    plt.close(fig)

    return out


def get_boxplot(f, out, x, y, cat, ymin, ymax):
    fig = plt.figure(clear=True)
    ax = plt.subplot(111)

    categories = json.loads(cat)
    df_filter = get_dataframe(f)[get_dataframe(f)[x].isin(categories)]
    data = [get_dataframe(f)[get_dataframe(f)[x] == category][y] for category in categories]
    
    if os.path.isfile('light.mplstyle'):
        mpl.rcParams.update(mpl.rcParamsDefault)
        plt.style.use('light.mplstyle')
        ax.boxplot(data)
        ax.set_xticklabels(categories)
        ax.set_ylabel(y)
    else:
        jtplot.style(theme='onedork')
        flierprops = dict(markerfacecolor='#aaaaaa33', markersize=5,
                linestyle='none')
        ax = sns.boxplot(x=df_filter[x], y=df_filter[y], flierprops=flierprops)

        for i, box in enumerate(ax.artists):
            box.set_edgecolor('#ddddddaa')
            # iterate over whiskers and median lines
            for j in range(6*i,6*(i+1)):
                ax.lines[j].set_color('#ddddddaa')

    ax.set_ylim(int(ymin), int(ymax))
    fig.savefig(out, format='png')
    plt.close(fig)

    return out


def get_infos(f, col):
    infos = { 
        'Column name': str(col),
    }
    infos.update(json.loads(get_dataframe(f)[col].describe().to_json()))

    return infos