ASS_DOMAIN_CONFIG = {
  # APP
  'host': 'localhost',
  'port': 5000,
  'debug': False,
  'data_folder': 'data/',
  'fig_filename': 'figures/temp.png',
  'conf_filename': 'conf/conf.json'
}